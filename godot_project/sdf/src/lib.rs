use gdnative::prelude::*;

mod core;

fn init(handle: InitHandle) {
    handle.add_class::<core::SDFCore>();
}

godot_init!(init);