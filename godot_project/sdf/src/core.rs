use gdnative::api::Resource;
use gdnative::prelude::*;


pub enum SDFType { //only primitives work here. gotta figure out operations
    Default(),
    Sphere { radius:f32 },
    Cube { size: Vector3 },
    Add{ a : Box<SDFType>, b : Box<SDFType> } , //box is a smart pointer
    Sub{ a : Box<SDFType>, b : Box<SDFType> } ,
}

impl SDFType {
    pub fn resolve(&self, point:Vector3)-> f32 {
        match self {
            SDFType::Default() => f32::INFINITY,
            SDFType::Sphere{radius} => point.length() - radius,
            SDFType::Cube{size} => f32::INFINITY,//todo
            SDFType::Add {a,b} => a.resolve(point).min(b.resolve(point)),
            SDFType::Sub{a,b} => b.resolve(point).max(-a.resolve(point)),
        }
    }
}



#[derive(NativeClass)]
#[inherit(Resource)]
pub struct SDFCore {
    data:SDFType
}

#[gdnative::methods]
impl SDFCore {
    fn new(_owner: &Resource) -> Self {
        Self { data : SDFType::Default() }
    }

    #[export]
    fn resolve(&self, _owner: &Resource, point:Vector3)->f32{
        self.data.resolve(point)
    }

    #[export]
    fn sphere(&self, _owner: &Resource, radius:f32)->SDFCore{
        Self {data:SDFType::Sphere{radius:radius}}
    }

    #[export]
    fn add(&self, _owner: &Resource, b:SDFCore)->SDFCore{
        Self {data:SDFType::Add{a:Box::new(self.data),b:Box::new(b.data)}}
    }


}