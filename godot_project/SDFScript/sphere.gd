extends ScriptSDF

class_name ScriptSDFSphere

var radius:float;

func resolve(point:Vector3)->float:
	return point.length() - radius;
