extends Resource

class_name ScriptSDF

func resolve(point:Vector3)->float:
	return INF

func sphere(radius:float)->ScriptSDF:
	var out = load("res://SDFScript/sphere.gd").new()
	out.radius = radius;
	return out;


func translate(translation:Vector3):
	var out = load("res://SDFScript/sphere.gd").new()
	out.translation = translation;
	return out;
	
func add(b:ScriptSDF):
