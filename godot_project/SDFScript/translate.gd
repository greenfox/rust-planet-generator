extends ScriptSDF

class_name ScriptSDFTranslate

var translation:Vector3;
var target:ScriptSDF;

func resolve(point:Vector3)->float:
	return target.resolve(point - translation);
